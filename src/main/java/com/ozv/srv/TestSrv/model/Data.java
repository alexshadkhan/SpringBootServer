package com.ozv.srv.TestSrv.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "data", indexes = { @Index(name = "idx_ip", columnList = "ip", unique = false),
		@Index(name = "idx_client_date", columnList = "clientDate", unique = false),
		@Index(name = "idx_server_date", columnList = "serverDate", unique = false),
		@Index(name = "idx_severity", columnList = "severity", unique = false), })
public class Data {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String ip;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date clientDate;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date serverDate;
	private Integer severity;

	@Column(columnDefinition = "TEXT")
	private String payload;

	public Data() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Data(Integer id, String ip, Date clientDate, Integer severity, String payload) {
		super();
		this.id = id;
		this.ip = ip;
		this.clientDate = clientDate;
		this.severity = severity;
		this.payload = payload;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Date getClientDate() {
		return clientDate;
	}

	public void setClientDate(Date clientDate) {
		this.clientDate = clientDate;
	}

	public Date getServerDate() {
		return serverDate;
	}

	public void setServerDate(Date serverDate) {
		this.serverDate = serverDate;
	}

	public Integer getSeverity() {
		return severity;
	}

	public void setSeverity(Integer severity) {
		this.severity = severity;
	}

	public String getPayload() {
		return payload;
	}

	public void setPayload(String payload) {
		this.payload = payload;
	}

}
