package com.ozv.srv.TestSrv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestSrvApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestSrvApplication.class, args);
	}
}
