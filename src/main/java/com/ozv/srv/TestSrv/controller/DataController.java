package com.ozv.srv.TestSrv.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ozv.srv.TestSrv.model.Data;
import com.ozv.srv.TestSrv.repo.DataRepository;

@RestController
public class DataController {

	@Autowired
	private DataRepository dataRepository;

	@GetMapping("/check")
	public String checkServer(@RequestParam(value="name", defaultValue="OK") String param)  {
		return "Rest Api Test:  " + param ;
	}

	@PostMapping(value = "/write")   
	public @ResponseBody String writeData (@RequestBody Data data	) {

		data.setServerDate(new Date());
		dataRepository.save(data);	
		return "Saved";
	}
}