package com.ozv.srv.TestSrv.repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.ozv.srv.TestSrv.model.Data;




// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete
public interface DataRepository extends CrudRepository<Data, Long> {
	List<Data> findByIp(String ip);
}
